* Structure of the modeling scripts
** ~/Kern~
   This directory contains the analysis scripts for the kern county data only.
   The prefaces of ${LIQ,CS,KernCty}$ indicate the source of the underlying
   landcover dataset.
   

* Modeling Notes
** Landcover deduplication
   In the events where a landcover class was too coarse and could include
   multiple CUP+ classes, we assume the class that produces the smaller
   summertime irrigation requirement. For example: lower $K_c$ value, shorter,
   and/or cooler growing season. This choice should bias this analysis toward
   being a lower-bound on the actual water footprint.
   + LIQ2014/LIQ2016
     - ~Corn, Sorghum, and Sudan~ was assigned to the ~Corn (Silage)~ class,
       since they are all fodder grasses. The other options were: ~Corn
       (grain)~, ~Corn (silage)~, and ~Sorghum~ 
     - ~Wheat~ was assigned to the ~Grains (small)~ CUP+ class. The other
       options were: ~Grains (winter)~ and ~Wheat~. The reason for this was for
       consistency with the CDL labels (see the ~crop_code_translations~
       spreadsheet). ~Wheat~ has a very similar $K_c$ to ~Grains (small)~, just
       a bit lower water requirement during the coldest part of the growing
       season (ref: the $K_c$ tables).
     - ~Onions and Garlic~ was assigned to the ~Garlic~ CUP+ class. The other
       options were: ~Onion (dry)~ and ~Garlic~. Garlic has a lower summertime
       irrigation requirement.
      ~Plums, Prunes, and Apricots~ was assigned to the ~Prunes~ CUP+ class. The other
       options were: ~Prunes~ and ~Stone fruit~. Prunes have a lower summertime
       irrigation requirement.
   + Kern2014/2016
     - ~WHEAT~ was assigned to the ~Grains (small)~ CUP+ class. The other
       options were: ~Grains (winter)~ and ~Wheat~. The reason for this was for
       consistency with the CDL labels (see the ~crop_code_translations~
       spreadsheet). ~Wheat~ has a very similar $K_c$ to ~Grains (small)~, just
       a bit lower water requirement during the coldest part of the growing
       season (ref: the $K_c$ tables).
